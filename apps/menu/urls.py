from django.urls import path, include
from apps.menu.views import ContactView, CategoryView, AboutUsView, \
    FAQView, PublicOfferView, PrivacyPolicyView, RequisitesView, FAQDetailView

urlpatterns = [
    path('menu/', include([
        path('category/', CategoryView.as_view()),
        path('contact/', ContactView.as_view()),
        path('about_us/', AboutUsView.as_view()),
        path('faq/', FAQView.as_view()),
        path('faq/<int:pk>/', FAQDetailView.as_view()),
        path('public_offer/', PublicOfferView.as_view()),
        path('privacy_policy/', PrivacyPolicyView.as_view()),
        path('requisites/', RequisitesView.as_view()),
    ]))
]