from django.contrib import admin
from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from apps.menu.models import SocialNetworks, Contact, AboutUs, FAQ, \
    Category, Subcategory, PublicOffer, PrivacyPolicy, Requisites


class SubcategoryInlines(admin.TabularInline):
    model = Subcategory
    fields = ('name',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    inlines = [SubcategoryInlines]


class SocialNetworksInlines(admin.TabularInline):
    model = SocialNetworks
    fields = ('icon', 'name')


@admin.register(Contact)
class ContactsAdmin(admin.ModelAdmin):
    inlines = [SocialNetworksInlines]

    def has_add_permission(self, request):
        obj_count = self.model.objects.count()
        if obj_count > 0:
            return False
        return super(ContactsAdmin, self).has_add_permission(request)


class AboutUsAdminForm(forms.ModelForm):
    text = forms.CharField(label='Текст', widget=CKEditorUploadingWidget())

    class Meta:
        model = AboutUs
        exclude = 0


@admin.register(AboutUs)
class AboutUsAdmin(admin.ModelAdmin):
    list_display = ['name', 'text']
    form = AboutUsAdminForm

    def has_add_permission(self, request):
        obj_count = self.model.objects.count()
        if obj_count > 0:
            return False
        return super(AboutUsAdmin, self).has_add_permission(request)


class PublicOfferAdminForm(forms.ModelForm):
    text = forms.CharField(label='Текст', widget=CKEditorUploadingWidget())

    class Meta:
        model = AboutUs
        exclude = 0


@admin.register(PublicOffer)
class PublicOfferAdmin(admin.ModelAdmin):
    list_display = ['name', 'text']
    form = PublicOfferAdminForm

    def has_add_permission(self, request):
        obj_count = self.model.objects.count()
        if obj_count > 0:
            return False
        return super(PublicOfferAdmin, self).has_add_permission(request)


class RequisitesAdminForm(forms.ModelForm):
    text = forms.CharField(label='Текст', widget=CKEditorUploadingWidget())

    class Meta:
        model = AboutUs
        exclude = 0


@admin.register(Requisites)
class RequisitesAdmin(admin.ModelAdmin):
    list_display = ['name', 'text']
    form = RequisitesAdminForm

    def has_add_permission(self, request):
        obj_count = self.model.objects.count()
        if obj_count > 0:
            return False
        return super(RequisitesAdmin, self).has_add_permission(request)


class PrivacyPolicyAdminForm(forms.ModelForm):
    text = forms.CharField(label='Текст', widget=CKEditorUploadingWidget())

    class Meta:
        model = AboutUs
        exclude = 0


@admin.register(PrivacyPolicy)
class PrivacyPolicyAdmin(admin.ModelAdmin):
    list_display = ['name', 'text']
    form = PrivacyPolicyAdminForm

    def has_add_permission(self, request):
        obj_count = self.model.objects.count()
        if obj_count > 0:
            return False
        return super(PrivacyPolicyAdmin, self).has_add_permission(request)


@admin.register(FAQ)
class FAQAdmin(admin.ModelAdmin):
    list_display = ['question', 'response']
