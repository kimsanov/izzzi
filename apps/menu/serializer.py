from rest_framework import serializers
from apps.menu.models import Category, Contact, Subcategory, AboutUs, \
    SocialNetworks, FAQ, Requisites, PrivacyPolicy, PublicOffer


class SubcategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Subcategory
        fields = (
            'name', 'id'
        )


class CategorySerializer(serializers.ModelSerializer):
    category_sub = SubcategorySerializer(allow_null=True, read_only=True,
                                         many=True)

    class Meta:
        model = Category
        fields = (
            'id', 'name', 'category_sub'
        )


class SocialNetworksSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialNetworks
        fields = (
            'icon', 'name'
        )


class ContactSerializer(serializers.ModelSerializer):
    social_contacts = SocialNetworksSerializer(allow_null=True, read_only=True,
                                               many=True)

    class Meta:
        model = Contact
        fields = (
            'name', 'descriptions', 'email', 'address', 'icon', 'phone_number',
            'length', 'width', 'social_contacts'
        )


class AboutUsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AboutUs
        fields = (
            'name', 'text'
        )


class FAQSerializer(serializers.ModelSerializer):
    class Meta:
        model = FAQ
        fields = (
            'id', 'question', 'response'
        )


class RequisitesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Requisites
        fields = (
            'name', 'text'
        )


class PrivacyPolicySerializer(serializers.ModelSerializer):
    class Meta:
        model = PrivacyPolicy
        fields = (
            'name', 'text'
        )


class PublicOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = PublicOffer
        fields = (
            'name', 'text'
        )
