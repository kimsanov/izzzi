from django.contrib import admin
from apps.coupon.models import Coupon, CouponImage, Address


class CouponImageInline(admin.TabularInline):
    model = CouponImage
    extra = 0


class AddressInline(admin.TabularInline):
    model = Address
    extra = 0


@admin.register(Coupon)
class CouponAdmin(admin.ModelAdmin):
    inlines = [CouponImageInline, AddressInline]
    list_display = ['company', 'about_coupon']
