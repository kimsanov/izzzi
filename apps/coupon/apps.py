from django.apps import AppConfig


class CouponConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.coupon'
    verbose_name = 'Купон'
    verbose_name_plural = 'Купоны'
