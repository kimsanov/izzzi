from django.db import models
from apps.coupon.utils import generate_filename
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
from apps.user.managers import UserManager


class User(AbstractUser):
    username = None
    email = None
    phone = PhoneNumberField(verbose_name='Номер телефона', unique=True)
    confirmation_code = models.CharField(
        verbose_name='Код', max_length=4, blank=True, null=True
    )
    confirmation_date = models.DateTimeField(
        verbose_name='Дата кода', auto_now=True
    )

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return str(self.phone)


class UserCompany(models.Model):

    user = models.OneToOneField(
        to='user.User', on_delete=models.SET_NULL, blank=True, null=True,
        verbose_name='Пользователь', related_name='creator_page'
    )
    photo = models.FileField(
        verbose_name='Загрузите аватар', upload_to=generate_filename,
        blank=True, null=True
    )
    name_company = models.CharField(
        verbose_name='Название компании', max_length=256, blank=True, null=True
    )
    company_description = models.TextField(
        verbose_name='Описание компании', blank=True, null=True,
        help_text='Например: чем занимается компания.'
    )
    # phone_number = models.CharField(verbose_name='Номер телефона',
    #                                 max_length=14)
    email = models.EmailField(verbose_name='E-mail')
    address = models.CharField(verbose_name='Адрес', max_length=256)
    instagram = models.CharField(
        verbose_name='Instagram', max_length=256, blank=True, null=True
    )
    facebook = models.CharField(
        verbose_name='Facebook', max_length=256, blank=True, null=True
    )
    vk = models.CharField(
        verbose_name='ВКонтакте', max_length=256, blank=True, null=True
    )
    whatsapp = models.CharField(
        verbose_name='Whatsapp', max_length=14, null=True, blank=True
    )
    telegram = models.CharField(
        verbose_name='Telegram', max_length=14, blank=True, null=True
    )
    date = models.DateField(verbose_name='Дата', auto_now_add=True)
    is_active = models.BooleanField(default=False, blank=True, null=True)

    class Meta:
        verbose_name = 'партнер'
        verbose_name_plural = 'партнеры'

    def __str__(self):
        return self.name_company


class Number(models.Model):
    phone = models.CharField(
        verbose_name='Номер телефона', max_length=14
    )
    company = models.ForeignKey(
        to='user.UserCompany', on_delete=models.CASCADE,
        verbose_name='Партнер', null=True, related_name='number_company'
    )
