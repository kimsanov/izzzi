from rest_framework.exceptions import APIException


class CouponDoesNotExist(APIException):
    status_code = 404
    default_detail = 'Not found'
