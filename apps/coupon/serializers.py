from rest_framework import serializers
from apps.coupon.models import Coupon, CouponImage, Address
from apps.user.models import UserCompany


class CouponImageSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(max_length=None, use_url=True)

    class Meta:
        model = CouponImage
        fields = ['image', 'id']


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ('address', 'phone_number', 'id')


class ShortUserCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserCompany
        fields = (
            'photo', 'name_company', 'id'
        )


class ActivePromotionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Coupon
        fields = ('id', 'about_coupon', 'quantity', 'is_activated')


class ActivatedPromotionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Coupon
        fields = (
            'id', 'about_coupon', 'is_activated_quantity', 'is_activated'
        )


class CouponListSerializer(serializers.ModelSerializer):
    company = ShortUserCompanySerializer(allow_null=True, many=False)
    price = serializers.IntegerField(read_only=True, allow_null=True)
    discount = serializers.IntegerField(read_only=True, allow_null=True)
    about_coupon = serializers.CharField(read_only=True)
    new_price = serializers.SerializerMethodField()
    # is_favorite = serializers.SerializerMethodField()

    class Meta:
        model = Coupon
        fields = (
            'discount', 'main_page', 'company', 'about_coupon', 'price',
            'new_price', 'id'
        )

    def get_new_price(self, obj):
        new_price = int(obj.price) - ((int(obj.price) *
                                           int(obj.discount) // 100))
        return new_price

    # def get_is_favorite(self, obj):
    #     request = self.context.get('request')
    #     coupons = request.user.user_favorite.filter(coupon=obj)
    #     for coupon in coupons:
    #         if coupon.coupon == obj:
    #             return True
    #     return False


class CouponDetailSerializer(serializers.ModelSerializer):
    company = ShortUserCompanySerializer(
        allow_null=True, many=False, read_only=True
    )
    start_date = serializers.DateField(format="%d-%m-%Y", read_only=True)
    end_date = serializers.DateField(format="%d-%m-%Y", read_only=True)
    coupon_images = CouponImageSerializer(
        allow_null=True, many=True, read_only=True
    )
    number_coupons_sold = serializers.IntegerField(
        allow_null=True, read_only=True, default=0
    )
    new_price = serializers.SerializerMethodField()
    sales_time = serializers.CharField(read_only=True, allow_null=True)
    category = serializers.CharField(
        source='category.name', read_only=True, allow_null=True
    )
    subcategory_name = serializers.CharField(
        source='subcategory.name', read_only=True, allow_null=True
    )
    coupon_address = AddressSerializer(
        read_only=True, many=True, allow_null=True
    )
    price = serializers.IntegerField(read_only=True, allow_null=True)
    discount = serializers.IntegerField(read_only=True, allow_null=True)

    class Meta:
        model = Coupon
        fields = (
            'company', 'main_page', 'discount', 'about_coupon', 'price',
            'new_price', 'category', 'subcategory_name',
            'start_date', 'end_date', 'number_coupons_sold',
            'sales_time', 'conditions', 'description', 'coupon_address',
            'coupon_images'
        )

    def get_new_price(self, request):
        new_price = int(request.price) - (int(request.price)
                                          * int(request.discount) // 100)
        return new_price
