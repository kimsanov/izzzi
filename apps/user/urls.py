from django.urls import path, include
from apps.user.views import RegisterView, ChangePasswordView, \
    MyProfileView, UserCompanyView, CreatorPageView, UserFavoriteView, \
    LogoutView, BuyCouponView, ChangePhoneNumberView, ActivateCouponView, \
    QRCodeView, LoginView, MyCouponsView, MyPromotionsView, FavoriteDetailView

urlpatterns = [
    path('user/', include([
        path('register/', RegisterView.as_view()),
        path('login/', LoginView.as_view()),
        path('logout/', LogoutView.as_view()),
        path('change_password/', ChangePasswordView.as_view(
            {'put': 'update'}
        )),
        path('change_phone_number/', ChangePhoneNumberView.as_view(
            {'put': 'update'}
        )),
        path('my_profile/', MyProfileView.as_view(
            {'get': 'retrieve', 'put': 'update'})),


        path('user_company/', UserCompanyView.as_view({'post': 'create'})),

        path('creator_page/', CreatorPageView.as_view({'get': 'retrieve'})),

        path('favorites/', UserFavoriteView.as_view({'get': 'list',
                                                     'post': 'create'})),
        path('favorites/<int:pk>/', FavoriteDetailView.as_view(
            {'get': 'retrieve', 'delete': 'destroy'}
             )),

        path('buy_coupon/', BuyCouponView.as_view({'post': 'create'})),
        path('buy_coupon/qr_code/<int:pk>/', QRCodeView.as_view({'get': 'list'})),
        path('activate_coupon/', ActivateCouponView.as_view()),

        path('my_coupons/', MyCouponsView.as_view({'get': 'retrieve'})),
        path('my_promotions/', MyPromotionsView.as_view({'get': 'retrieve'}))

    ]))
]
