from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from apps.coupon.service import PaginationCoupons, CouponFilter
from apps.coupon.models import Coupon
from apps.coupon.serializers import CouponListSerializer, \
    CouponDetailSerializer
from rest_framework import filters
from apps.coupon.exceptions import CouponDoesNotExist
from django.db.models import Q
from django.utils import timezone


class CouponListView(ModelViewSet):
    serializer_class = CouponListSerializer
    permission_classes = (AllowAny,)
    pagination_class = PaginationCoupons
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = CouponFilter
    ordering_fields = ['price', 'about_coupon']

    def get_queryset(self):
        now = timezone.now()
        coupons = Coupon.objects.filter(end_date__lt=now).update(is_active=False)
        query = self.request.GET.get('search')
        if query:
            return Coupon.objects.filter(is_activated=True).filter(
                Q(price__icontains=query) |
                Q(discount__icontains=query) |
                Q(about_coupon__icontains=query) |
                Q(company__name_company__icontains=query)
            )
        else:
            return Coupon.objects.filter(is_activated=True)


class CouponDetailView(ModelViewSet):
    queryset = Coupon.objects.all()
    serializer_class = CouponDetailSerializer
    permission_classes = (AllowAny,)
    lookup_field = 'pk'

    def get_object(self):
        try:
            return Coupon.objects.get(pk=self.kwargs['pk'])
        except Coupon.DoesNotExist:
            raise CouponDoesNotExist


class SimilarCouponsView(ModelViewSet):
    serializer_class = CouponListSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        coupon = Coupon.objects.get(pk=self.kwargs['pk'])
        subcategory = coupon.subcategory
        print(subcategory)
        similar_products = Coupon.objects.filter(subcategory=subcategory)\
            .exclude(pk=coupon.pk)
        return similar_products
