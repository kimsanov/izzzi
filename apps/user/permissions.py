from rest_framework import permissions


class IsOwnerCoupon(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return True
        else:
            return obj.user == request.user
