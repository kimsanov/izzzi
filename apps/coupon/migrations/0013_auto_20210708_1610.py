# Generated by Django 3.2.4 on 2021-07-08 10:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coupon', '0012_auto_20210708_1601'),
    ]

    operations = [
        migrations.RenameField(
            model_name='buyercoupon',
            old_name='is_activate',
            new_name='is_activated',
        ),
        migrations.AlterField(
            model_name='buyercoupon',
            name='date',
            field=models.DateTimeField(verbose_name='Активирован в'),
        ),
    ]
