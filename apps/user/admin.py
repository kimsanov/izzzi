from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _

from apps.user.models import User, UserCompany, Number
from apps.coupon.models import UserFavoriteCoupon, BuyCoupon


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {'fields': ('phone', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name',
                                         'confirmation_code')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser'
                                       )}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'password1', 'password2'),
        }),
    )
    list_display = ('phone', 'first_name', 'last_name', 'is_active')
    search_fields = ('phone', 'first_name', 'last_name')
    ordering = ('phone',)


class NumberInline(admin.TabularInline):
    model = Number
    extra = 1


@admin.register(UserCompany)
class UserCompanyAdmin(admin.ModelAdmin):
    inlines = [NumberInline]
    list_display = ['name_company', 'user', 'date', 'is_active']


@admin.register(UserFavoriteCoupon)
class UserFavoriteCouponAdmin(admin.ModelAdmin):
    list_display = ['user', 'coupon']


@admin.register(BuyCoupon)
class BuyCouponAdmin(admin.ModelAdmin):
    list_display = ['user', 'coupon', 'date']
