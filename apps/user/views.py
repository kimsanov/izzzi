from django.contrib import auth
from rest_framework import generics
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authentication import TokenAuthentication, \
    SessionAuthentication
from apps.user.models import User, UserCompany
from apps.user.serializers import RegisterSerializer, \
    ChangePasswordSerializer, UserSerializer, UserCompanySerializer, \
    CreatorPageSerializer, UserFavoriteCouponsSerializer, \
    BuyCouponSerializer, ChangePhoneNumberSerializer, \
    LoginSerializer, QrCodeSerializer, MyCouponsSerializer, \
    MyPromotionsSerializer, UserFavoriteCouponsDetailSerializer
from apps.coupon.models import UserFavoriteCoupon, BuyCoupon, Coupon
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework import status
from apps.user.exceptions import UserFavoriteCouponDoesNotExist, \
    QrCodeDoesNotExist, NotValidQRToken, MissingQueryParamsException,\
    InvalidToken
from rest_framework.viewsets import ModelViewSet
from django.contrib.auth.signals import user_logged_out


class RegisterView(generics.GenericAPIView):
    serializer_class = RegisterSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token = Token.objects.create(user=user).save()
        return Response({'detail': 'Регистрация прошла успешно'},
                        status=status.HTTP_201_CREATED)


class LoginView(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer

    def post(self, request):
        phone = request.data['phone']
        password = request.data['password']

        user = auth.authenticate(phone=phone, password=password)
        if user:
            token, created = Token.objects.get_or_create(user=user)
            return Response({'detail': token.key},
                            status=status.HTTP_200_OK)
        else:
            return Response(
                {'detail': 'Неправильный номер или пароль!'},
                status=status.HTTP_404_NOT_FOUND)


class LogoutView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def post(self, request, *args, **kwargs):
        request._auth.delete()
        user_logged_out.send(
            sender=request.user.__class__, request=request, user=request.user
        )
        return Response({'detail': 'ок'}, status=status.HTTP_204_NO_CONTENT)


class ChangePasswordView(ModelViewSet):
    serializer_class = ChangePasswordSerializer
    queryset = User.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class ChangePhoneNumberView(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = ChangePhoneNumberSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class MyProfileView(ModelViewSet):
    queryset = User.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer
    model = User

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def get_object(self):
        return self.request.user


class UserCompanyView(ModelViewSet):
    queryset = UserCompany.objects.all()
    serializer_class = UserCompanySerializer
    permission_classes = (IsAuthenticated,)


class CreatorPageView(ModelViewSet):
    queryset = UserCompany.objects.all()
    serializer_class = CreatorPageSerializer
    lookup_field = 'pk'

    def get_object(self):
        return self.request.user


class UserFavoriteView(ModelViewSet):
    queryset = UserFavoriteCoupon.objects.all()
    model = UserFavoriteCoupon
    serializer_class = UserFavoriteCouponsSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication,)
    lookup_field = 'pk'

    def get_object(self):
        try:
            return UserFavoriteCoupon.objects.get(pk=self.kwargs['pk'])
        except UserFavoriteCoupon.DoesNotExist:
            raise UserFavoriteCouponDoesNotExist

    def get_queryset(self):
        self.queryset = UserFavoriteCoupon.objects.all()
        return self.queryset.filter(user_id=self.request.user)


class FavoriteDetailView(ModelViewSet):
    queryset = UserFavoriteCoupon.objects.all()
    serializer_class = UserFavoriteCouponsDetailSerializer
    lookup_field = 'pk'


class BuyCouponView(ModelViewSet):
    queryset = BuyCoupon.objects.all()
    serializer_class = BuyCouponSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class QRCodeView(ModelViewSet):
    serializer_class = QrCodeSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    lookup_field = 'pk'

    def get_object(self):
        try:
            return BuyCoupon.objects.filter(pk=self.kwargs['pk'])
        except BuyCoupon.DoesNotExist:
            raise QrCodeDoesNotExist

    def get_queryset(self):
        buy_coupon = self.get_object()
        return buy_coupon


class ActivateCouponView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def get(self, request, *args, **kwargs):
        coupon_id = self.request.query_params.get('coupon')
        return coupon_id


class MyCouponsView(ModelViewSet):
    queryset = BuyCoupon.objects.all()
    serializer_class = MyCouponsSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def get_object(self):
        return self.request.user


class MyPromotionsView(ModelViewSet):
    queryset = Coupon.objects.all()
    serializer_class = MyPromotionsSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def get_object(self):
        return self.request.user
