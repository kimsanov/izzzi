from rest_framework.exceptions import APIException


class HelpDoesNotExist(APIException):
    status_code = 404
    default_detail = 'Not found'


class CategoryDoesNotExist(APIException):
    status_code = 404
    default_detail = 'Not found'
