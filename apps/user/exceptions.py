from rest_framework.exceptions import APIException


class UserFavoriteCouponDoesNotExist(APIException):
    status_code = 404
    default_detail = 'Такой купон нет у вас в избранном'


class UserFavoriteCouponDoesExist(APIException):
    status_code = 404
    default_detail = 'Такой купон уже есть у вас в избранном'


class QrCodeDoesNotExist(APIException):
    status_code = 404
    default_detail = 'Not found'


class NotValidQRToken(APIException):
    status_code = 400
    default_detail = 'Not valid qr token'


class MissingQueryParamsException(APIException):
    status_code = 400
    default_detail = 'Not valid qr token'


class InvalidToken(APIException):
    status_code = 400
    default_detail = 'Invalid Token'

class PermissionDenied(APIException):
    status_code = 400
    default_detail = 'Вы не являетесь партнером'

