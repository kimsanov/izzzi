from django.db import models
from apps.coupon.utils import generate_filename


class Category(models.Model):

    name = models.CharField(verbose_name='Категория', max_length=256)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Subcategory(models.Model):

    name = models.CharField(verbose_name='Подкатегория', max_length=256)
    category = models.ForeignKey(
        to='menu.Category', verbose_name='Категория', null=True,
        on_delete=models.SET_NULL, related_name='category_sub'
    )

    class Meta:
        verbose_name = 'Подкатегория'
        verbose_name_plural = 'Подкатегории'

    def __str__(self):
        return self.name


class Contact(models.Model):

    name = models.CharField(
        verbose_name='Название', max_length=256, default='Контакты'
    )
    descriptions = models.TextField(verbose_name='Текст')
    email = models.EmailField(verbose_name='Email:')
    address = models.CharField(verbose_name='Наш адрес', max_length=256)
    icon = models.FileField(verbose_name='Иконка', upload_to=generate_filename)
    phone_number = models.CharField(
        verbose_name='Наши телефоны:', max_length=15
    )
    length = models.FloatField(verbose_name='Длина', default=0)
    width = models.FloatField(verbose_name='Ширина', default=0)

    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'

    def __str__(self):
        return f'{self.name}'


class SocialNetworks(models.Model):

    icon = models.FileField(verbose_name='Икон', upload_to=generate_filename)
    name = models.CharField(verbose_name='Название', max_length=256)
    social_networks = models.ForeignKey(
        Contact, verbose_name='Мы в социальных сетях:',
        on_delete=models.CASCADE, related_name='social_contacts'
    )


class AboutUs(models.Model):

    name = models.CharField(
        verbose_name='Название', max_length=256, default='О нас'
    )
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'О нас'
        verbose_name_plural = 'О нас'

    def __str__(self):
        return f'{self.name}'


class FAQ(models.Model):

    question = models.CharField(verbose_name='Вопрос', max_length=256)
    response = models.TextField(verbose_name='Ответ')

    class Meta:
        verbose_name = 'Вопрос и ответ'
        verbose_name_plural = 'Вопросы и ответы'
        ordering = ['-id']

    def __str__(self):
        return f'{self.question}'


class PrivacyPolicy(models.Model):
    name = models.CharField(
        verbose_name='Название', max_length=256,
        default='Политика конфиденциальности'
    )
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Политика конфиденциальности'
        verbose_name_plural = 'Политика конфиденциальности'

    def __str__(self):
        return self.name


class PublicOffer(models.Model):
    name = models.CharField(
        verbose_name='Название', max_length=256, default='Публичная оферта'
    )
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Публичная оферта'
        verbose_name_plural = 'Публичная оферта'

    def __str__(self):
        return self.name


class Requisites(models.Model):
    name = models.CharField(
        verbose_name='Название', max_length=256, default='Реквизиты'
    )
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Реквизиты'
        verbose_name_plural = 'Реквизиты'

    def __str__(self):
        return self.name
