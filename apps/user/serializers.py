from rest_framework import serializers
from apps.user.exceptions import UserFavoriteCouponDoesExist
from apps.user.models import User, UserCompany, Number
from phonenumber_field.serializerfields import PhoneNumberField
from apps.coupon.serializers import CouponListSerializer, Coupon, \
    ActivePromotionSerializer, ActivatedPromotionSerializer, \
    CouponDetailSerializer
from apps.coupon.models import UserFavoriteCoupon, BuyCoupon


class UserSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = (
            'first_name', 'last_name', 'phone', 'id'
        )

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.save()
        return instance


class RegisterSerializer(serializers.ModelSerializer):
    phone = PhoneNumberField(required=True)
    password = serializers.CharField(
        write_only=True, required=True, style={'input_type': 'password'}
    )
    password2 = serializers.CharField(
        write_only=True, required=True, style={'input_type': 'password'}
    )

    class Meta:
        model = User
        fields = (
         'first_name', 'last_name', 'phone', 'password', 'password2', 'id'
        )
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True}
        }

    def validate_phone(self, phone):
        if User.objects.filter(phone=phone):
            raise serializers.ValidationError(
                'Пользователь с таким номером ужe существует'
            )
        return phone

    def validate(self, user):
        if user['password'] != user['password2']:
            raise serializers.ValidationError('Пароль не совпадает')
        return user

    def validate_password(self, password):
        if len(password) < 8:
            raise serializers.ValidationError(
                'Пароль должен содержать не менее 8 символов'
            )
        return password

    def create(self, validated_data):
        user = User.objects.create(
            phone=validated_data['phone'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class LoginSerializer(serializers.ModelSerializer):
    phone = PhoneNumberField(write_only=True, required=True)
    password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('phone', 'password')


class ChangePasswordSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(write_only=True, required=True)
    password = serializers.CharField(write_only=True, required=True)
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            'old_password', 'password', 'password2'
        )

    def validate(self, user):
        if user['password'] != user['password2']:
            raise serializers.ValidationError('Пароль не совпадает')
        return user

    def validate_old_password(self, value):
        user = self.context['request'].user
        if not user.check_password(value):
            raise serializers.ValidationError('Неправильный старый пароль')
        return value

    def validate_password(self, password):
        if len(password) < 8:
            raise serializers.ValidationError(
                'Пароль должен содержать не менее 8 символов'
            )
        return password

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        return instance


class ChangePhoneNumberSerializer(serializers.ModelSerializer):
    new_phone_number = PhoneNumberField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            'new_phone_number',
        )

    def update(self, instance, validated_data):
        instance.phone = self.validated_data['new_phone_number']
        if User.objects.filter(phone=instance.phone):
            raise serializers.ValidationError('Неправильный номер')
        instance.save()
        return instance


class NumberSerializer(serializers.ModelSerializer):
    phone = PhoneNumberField(required=False)

    class Meta:
        model = Number
        fields = (
            'phone'
        )


class UserCompanySerializer(serializers.ModelSerializer):
    date = serializers.DateField(format="%d.%m.%Y", read_only=True)

    class Meta:
        model = UserCompany
        fields = (
            'user', 'photo', 'name_company', 'company_description',
            'email', 'address', 'instagram', 'facebook', 'vk', 'number_company',
            'whatsapp', 'telegram', 'date', 'id'
        )

    def create(self, validated_data):
        user = self.context.get('request').user
        user_company = UserCompany.objects.create(user=user, **validated_data)
        user_company.save()
        return user_company


class CreatorPageSerializer(serializers.ModelSerializer):
    creator_page = UserCompanySerializer(
        read_only=True, allow_null=True
    )
    company_discounts = serializers.SerializerMethodField()

    class Meta:
        model = UserCompany
        fields = (
            'creator_page', 'company_discounts'
        )

    def get_company_discounts(self, request):
        company = UserCompany.objects.get(user=request)
        discount = Coupon.objects.filter(company=company)
        serializer = CouponListSerializer(discount, many=True)
        return serializer.data


class UserFavoriteCouponsSerializer(serializers.ModelSerializer):
    coupon = CouponListSerializer(required=False, read_only=True)
    coupon_id = serializers.IntegerField()

    class Meta:
        model = UserFavoriteCoupon
        fields = (
            'user', 'coupon_id', 'coupon', 'id'
        )

    def validate(self, data):
        coupon = data.get('coupon_id', None)
        user = self.context.get('request').user
        if UserFavoriteCoupon.objects.filter(coupon=coupon, user=user):
            raise UserFavoriteCouponDoesExist
        return data

    def create(self, validated_data):
        user = self.context.get('request').user
        coupon_id = self.validated_data.get('coupon_id')
        coupon = Coupon.objects.get(id=coupon_id)
        user_favorite = UserFavoriteCoupon.objects.create(user=user,
                                                          coupon=coupon,
                                                          **validated_data)
        user_favorite.save()
        return user_favorite


class UserFavoriteCouponsDetailSerializer(serializers.ModelSerializer):
    coupon = CouponDetailSerializer(read_only=True)

    class Meta:
        model = Coupon
        fields = ('coupon', )


class QrCodeSerializer(serializers.ModelSerializer):
    qr_code = serializers.ImageField(max_length=None, use_url=True)

    class Meta:
        model = BuyCoupon
        fields = ('qr_code',)


class BuyCouponSerializer(serializers.ModelSerializer):

    class Meta:
        model = BuyCoupon
        fields = (
            'user', 'coupon', 'qr_code', 'date', 'id'
        )

    def create(self, validated_data):
        user = self.context.get('request').user
        qr_code = BuyCoupon.objects.create(user=user, **validated_data)
        qr_code.save()
        return qr_code


class MyCouponsSerializer(serializers.ModelSerializer):
    is_active = serializers.SerializerMethodField()
    is_activated = serializers.SerializerMethodField()
    expired = serializers.SerializerMethodField()

    class Meta:
        model = Coupon
        fields = (
            'is_active', 'is_activated', 'expired'
        )

    def get_is_active(self, request):
        request = self.context.get('request').user
        print(request)
        active = Coupon.objects.filter(coupon__user__buycoupon=request.id,
                                       is_activated=True, is_active=True)
        serializer = CouponListSerializer(active, many=True)
        return serializer.data

    def get_is_activated(self, request):
        request = self.context.get('request').user
        is_activated = Coupon.objects.filter(coupon__user__buycoupon=request.id,
                                             is_activated=True, is_active=True,
                                             is_activated_user=True)
        serializer = CouponListSerializer(is_activated, many=True)
        return serializer.data

    def get_expired(self, request):
        request = self.context.get('request').user
        is_activated = Coupon.objects.filter(coupon__user__buycoupon=request.id,
                                             is_active=False)
        serializer = CouponListSerializer(is_activated, many=True)
        return serializer.data


class MyPromotionsSerializer(serializers.ModelSerializer):
    is_active = serializers.SerializerMethodField()
    is_activated = serializers.SerializerMethodField()
    expired = serializers.SerializerMethodField()

    class Meta:
        model = Coupon
        fields = (
            'is_active', 'is_activated', 'expired'
        )

    def get_is_active(self, request):
        company = UserCompany.objects.get(user=request)
        coupon = Coupon.objects.filter(company=company, is_active=True,
                                       is_activated=False)
        serializer = ActivePromotionSerializer(coupon, many=True)
        return serializer.data

    def get_is_activated(self, request):
        company = UserCompany.objects.get(user=request)
        coupon = Coupon.objects.filter(company=company, is_active=True,
                                       is_activated=True)
        serializer = ActivatedPromotionSerializer(coupon, many=True)
        return serializer.data

    def get_expired(self, request):
        company = UserCompany.objects.get(user=request)
        coupon = Coupon.objects.filter(company=company, is_active=False)
        serializer = ActivatedPromotionSerializer(coupon, many=True)
        return serializer.data
