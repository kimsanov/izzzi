from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
   openapi.Info(
      title="izzzi API",
      default_version='v1',
      description="izzzi Swagger",
      terms_of_service="",
      contact=openapi.Contact(email=""),
      license=openapi.License(name=""),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
    path(route='admin/', view=admin.site.urls),
    path(route='silk/', view=include(arg='silk.urls')),
    path(route='api/v1/', view=include(arg='apps.user.urls')),
    path(route='api/v1/', view=include(arg='apps.menu.urls')),
    path(route='api/v1/', view=include(arg='apps.coupon.urls')),
    path('chaining/', include('smart_selects.urls')),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0),
         name='schema-swagger-ui'),
    path("ckeditor/", include("ckeditor_uploader.urls")),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
