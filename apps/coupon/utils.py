import hashlib
import os
import re
import uuid

from django.utils import timezone


def slygify_camelcase(string: str, sep: str = '-') -> str:
    repl = r'\1{}2'.format(sep)
    s1 = re.sub('(.)([A-Z][a-z]+)', repl, string)
    return re.sub('([a-z0-9])([A-Z])', repl, s1)


def generate_filename(instance, filename: str) -> str:
    f, ext = os.path.splitext(filename)
    model_name = slygify_camelcase(instance._meta.model.__name__, '_')
    strftime = timezone.datetime.now().strftime('%Y/%m/%d')
    hex_ = uuid.uuid4().hex

    return f'{model_name}/{strftime}/{hex_}{ext}'


def image_upload_to(instance, filename):
    basename = os.path.basename(filename)
    root, ext = os.path.splitext(basename)
    hash_object = hashlib.md5(f"{root}".encode())
    return f'coupon/{hash_object.hexdigest()}{ext}{id}'
