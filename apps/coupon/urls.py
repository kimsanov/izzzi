from django.urls import path, include
from apps.coupon.views import CouponListView, CouponDetailView, \
    SimilarCouponsView

urlpatterns = [
    path('coupon/', include([
        path('', CouponListView.as_view({'get': 'list'})),
        path('<int:pk>/', CouponDetailView.as_view({'get': 'retrieve'})),
        path('similar_coupons/<int:pk>/', SimilarCouponsView.as_view(
            {'get': 'list'}))
    ]))
]
