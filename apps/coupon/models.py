from django.db import models
from smart_selects.db_fields import ChainedForeignKey
from apps.coupon.utils import generate_filename, image_upload_to
import qrcode
from io import BytesIO
from django.core.files import File
from PIL import Image, ImageDraw


class Coupon(models.Model):
    company = models.ForeignKey(
        to='user.UserCompany', on_delete=models.CASCADE,
        verbose_name='Создатель', null=True, related_name='company'
    )
    category = models.ForeignKey(
        to='menu.Category', verbose_name='Категория', on_delete=models.CASCADE,
        null=True
    )
    subcategory = ChainedForeignKey(
        to='menu.Subcategory', chained_field='category', show_all=False,
        chained_model_field='category',  null=True, on_delete=models.CASCADE
    )
    discount = models.DecimalField(
        verbose_name='Купон на скидку %', max_digits=10, decimal_places=2
    )
    about_coupon = models.CharField(verbose_name='О купоне', max_length=256)
    main_page = models.FileField(
        verbose_name='Основное изображение', null=True,
        upload_to=generate_filename, blank=True
    )
    price = models.DecimalField(
        verbose_name='Цена', max_digits=10, decimal_places=2
    )
    start_date = models.DateField(
        verbose_name='Период действия акции c', blank=True, null=True
    )
    end_date = models.DateField(
        verbose_name='Период действия акции по', blank=True, null=True
    )
    quantity = models.PositiveIntegerField(
        verbose_name='Количество купонов', default=0, blank=True, null=True
    )
    number_coupons_sold = models.PositiveIntegerField(
        verbose_name='Количество проданных купонов', default=0, blank=True,
        null=True)
    sales_time = models.CharField(
        verbose_name='Время продаж', max_length=256,
        default='Время продаж ограничено!'
    )
    conditions = models.TextField(verbose_name='Условия')
    description = models.TextField(verbose_name='Описание')
    is_active = models.BooleanField(default=True, verbose_name='is_active')
    is_activated = models.BooleanField(
        default=False, verbose_name='is_activated'
    )
    is_activated_user = models.BooleanField(
        default=False,
        verbose_name='is_activated_user'
    )
    is_activated_quantity = models.PositiveIntegerField(
        verbose_name='Активированные', default=0
    )

    class Meta:
        verbose_name = 'Купон'
        verbose_name_plural = 'Купоны'
        ordering = ['-id']

    def __str__(self):
        return f'{self.about_coupon}'


class Address(models.Model):
    address = models.CharField(verbose_name='Адрес', max_length=256)
    phone_number = models.CharField(
        verbose_name='Номер телефона', max_length=256
    )
    coupon = models.ForeignKey(
        to='coupon.Coupon', on_delete=models.CASCADE, verbose_name='Акция',
        related_name='coupon_address'
    )

    class Meta:
        verbose_name = 'Адрес'
        verbose_name_plural = 'Адрес'

    def __str__(self):
        return self.address


class CouponImage(models.Model):

    image = models.ImageField(
        verbose_name='Фото акции', upload_to=generate_filename, blank=True,
        null=True
    )
    coupon = models.ForeignKey(
        to='coupon.Coupon', on_delete=models.CASCADE, default=1, blank=True,
        null=True, related_name='coupon_images'
    )

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'

    def __str__(self):
        return str(self.image.url)


class UserFavoriteCoupon(models.Model):
    user = models.ForeignKey(
        to='user.User', on_delete=models.SET_NULL, verbose_name='Пользователь',
        null=True, blank=True, related_name='user_favorite'
    )
    coupon = models.ForeignKey(
        to='coupon.Coupon', on_delete=models.SET_NULL, null=True,
        verbose_name='Акции', blank=True, related_name='favorite_discount'
    )

    class Meta:
        verbose_name = 'Купон'
        verbose_name_plural = 'Избранные купоны пользователя'

    def __str__(self):
        return f'{self.coupon}'


class BuyCoupon(models.Model):
    user = models.ForeignKey(
        to='user.User', verbose_name='Пользователь', on_delete=models.SET_NULL,
        null=True
    )
    coupon = models.ForeignKey(
        to='coupon.Coupon', verbose_name='Акция', on_delete=models.SET_NULL,
        null=True, related_name='coupon'
    )
    qr_code = models.ImageField(
        upload_to=image_upload_to, blank=True, verbose_name='qr_code',
        null=True
    )
    date = models.DateTimeField(
        verbose_name='Активирован в', blank=True, auto_now=True
    )

    class Meta:
        verbose_name = 'Купленный купон'
        verbose_name_plural = 'Купленные купоны'

    def __str__(self):
        return f'{self.coupon.coupon}'

    def save(self, *args, **kwargs):
        qrcode_img = qrcode.make(self.coupon)
        canvas = Image.new('RGB', (qrcode_img.pixel_size,
                                   qrcode_img.pixel_size), 'white')
        draw = ImageDraw.Draw(canvas)
        canvas.paste(qrcode_img)
        fname = f'qr_code-{self.coupon}' + '.png'
        buffer = BytesIO()
        canvas.save(buffer, 'PNG')
        self.qr_code.save(fname, File(buffer), save=False)
        canvas.close()
        super(BuyCoupon, self).save(*args, **kwargs)
