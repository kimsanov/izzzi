from rest_framework.permissions import AllowAny
from rest_framework import generics
from apps.menu.exceptions import HelpDoesNotExist, CategoryDoesNotExist
from apps.menu.models import Contact, Category, AboutUs, FAQ, Requisites,\
    PublicOffer, PrivacyPolicy
from apps.menu.serializer import ContactSerializer, CategorySerializer,\
    AboutUsSerializer, FAQSerializer, RequisitesSerializer,\
    PublicOfferSerializer, PrivacyPolicySerializer


class CategoryView(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    model = Category
    permission_classes = (AllowAny,)

    def get_object(self):
        try:
            return Category.objects.get(pk=self.kwargs['pk'])
        except Category.DoesNotExist:
            raise CategoryDoesNotExist


class ContactView(generics.RetrieveAPIView):
    model = Contact
    serializer_class = ContactSerializer
    permission_classes = (AllowAny,)

    def get_object(self):
        return Contact.objects.first()


class AboutUsView(generics.RetrieveAPIView):
    serializer_class = AboutUsSerializer
    model = AboutUs
    permission_classes = (AllowAny,)

    def get_object(self):
        return AboutUs.objects.first()


class FAQView(generics.ListAPIView):
    model = FAQ
    queryset = FAQ.objects.all()
    lookup_field = 'pk'
    serializer_class = FAQSerializer
    permission_classes = (AllowAny,)

    def get_object(self):
        try:
            return FAQ.objects.get(pk=self.kwargs['pk'])
        except FAQ.DoesNotExist:
            raise HelpDoesNotExist


class FAQDetailView(generics.RetrieveAPIView):
    model = FAQ
    queryset = FAQ.objects.all()
    serializer_class = FAQSerializer
    permission_classes = (AllowAny,)

    def get_object(self):
        return FAQ.objects.first()


class RequisitesView(generics.RetrieveAPIView):
    serializer_class = RequisitesSerializer
    model = Requisites
    permission_classes = (AllowAny,)

    def get_object(self):
        return Requisites.objects.first()


class PublicOfferView(generics.RetrieveAPIView):
    serializer_class = PublicOfferSerializer
    model = PublicOffer
    permission_classes = (AllowAny,)

    def get_object(self):
        return PublicOffer.objects.first()


class PrivacyPolicyView(generics.RetrieveAPIView):
    serializer_class = PrivacyPolicySerializer
    model = PrivacyPolicy
    permission_classes = (AllowAny,)

    def get_object(self):
        return PrivacyPolicy.objects.first()
